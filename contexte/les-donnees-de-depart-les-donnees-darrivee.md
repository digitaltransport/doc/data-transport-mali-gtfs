# Les données de départ, les données d'arrivée

----------

## Données de départ

Pour mieux comprendre le cheminement qui a été fait pour parvenir à un GTFS lors de ce sprint, voici les données dont disposait Billet Express au départ (déjà collectées auprès des différentes compagnies) :

| Id compagnie | Compagnie | Id Ville depart | Ville Départ | Id Ville Arrivée | Ville Arrivée | Id heure | Heure | Id Gare | Gare | Prix | Date départ |
|---|---|---|---|---|---|---|---|---|---|---|---|
| 8 | BANI | 11 | Bamako | 27 | San | 19 | 5H | 8 | Bamako Sogoniko | 6300 | 2018/08/01 |
| 8 | BANI | 11 | Bamako | 27 | San | 19 | 5H | 8 | Bamako Sogoniko | 6300 | 2018/08/02 |
| 8 | BANI | 11 | Bamako | 27 | San | 19 | 5H | 8 | Bamako Sogoniko | 6300 | 2018/08/02 |

## Données à l'arrivée

A la fin de la semaine, nous disposions d'un GTFS (archive ZIP), composé des fichiers suivants :

### Fichier des compagnies

| agency_id | agency_name  | agency_url | agency_timezone |
|---|---|---|---|
| BAN | BANI | http://billetexpress.ml | Africa/Bamako |

### Fichier des arrêts

| stop_id | stop_name | stop_lat | stop_lon |
|---|---|---|---|
| bamako1 | Bamako Magnambougou | 12.6018181 | -7.9656551 |

### Fichier des routes

| route_id | agency_id | route_short_name | route_long_name |route_type|
|---|---|---|---|---|
| ABC001 | ABC | 1 | bamako_koutiala | 3 |


### Fichier des trajets

| route_id | service_id | trip_id | shape_id |route_type|
|---|---|---|---|---|
| ABC001 | lun-mar-mer-jeu-ven-sam | ABC001001 | shape-bam-kou |

### Fichier des heures de passage

| trip-id | arrival_time | departure_time | stop_id |stop_sequence|
|---|---|---|---|---|
| koutiala9h | 09:00:00 | 09:00:00 | bamako1 | 1 |
| koutiala9h | 25:00:00 | 25:00:00 | koutiala1 | 2 |


### Fichier des calendriers de services

| service_id | monday | tuesday | wednesday |thursday | friday | saturday | sunday | 
|---|---|---|---|---|---|---|---|
| lun-mar-mer-jeu-ven-sam | 1 | 1 | 1 | 1 | 1 | 1 | 0 |


