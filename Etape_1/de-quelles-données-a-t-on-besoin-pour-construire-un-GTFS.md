# De quelles données a-t-on besoin pour construire un GTFS ?

----------

> Je gère un réseau de lignes de bus, mais je ne dispose d'aucune donnée informatique sur ce dernier : quels sont les prérequis pour construire un GTFS ? 


Il s'agit en premier temps de savoir quelles sont les données à collecter avant de pouvoir commencer à construire un GTFS :
* Les compagnies : nom, fuseau horaire, site internet
* Les arrêts : nom de l'arrêt, coordonnées (lat, lon)
* Les lignes : les points de passages (arrêts d'origine, arrêts intermédiaires, arrêt de destination), la compagnie qui opère sur cette ligne, éventuellement, les tracés (shapefile) des lignes
* Les fréquences : les différents calendriers des lignes (tous les jours, jeudi et vendredi, etc...)
* Les jours exceptionnels (jours fériés, fêtes, etc...), les dates de validité du calendrier 
* Les horaires de passage : les heures de passages à chaque arrêt 

> Dans cette documentation, nous nous concentrons sur la production d'un fichier GTFS "minimal". C'est à dire que les différentes étapes qui sont décrites aboutissent vers un fichier GTFS réutilisable, et pouvant générer l'essentiel des services, mais pour obtenir un fichier le plus complet possible, la documentation de Google vous permettra d'aller plus loin. 


Le GTFS comprend des informations obligatoires (indispensables pour qu'il puisse être utilisé), et d'autres facultatives, mais qui peuvent présenter un fort intérêt pour de futurs services. 

Exemple d'informations obligatoires : localisation des arrêts, horaires de passage, nom de la compagnie

Exemple d'informations facultatives : couleur de la ligne, accessibilité de l'arrêt aux personnes à mobilité réduite, tarifs