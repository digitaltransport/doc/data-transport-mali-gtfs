# Qu'est-ce que le GTFS feed validator ?

--------

Lorsqu'il est possible d'exporter un premier extrait du GTFS, il est important de le tester dès que possible, afin de corriger d'éventuelles erreurs. Par la suite, il est recommandé de tester le fichier régulièrement, pour éviter de trop longues corrections. 

**Le GTFS Feed Validator est disponible à cette adresse : https://gtfsfeedvalidator.transitscreen.com** 

