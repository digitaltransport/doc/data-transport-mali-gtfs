# Important avant de commencer la saisie

----------

### 1/ Le logiciel permet d'archiver des sauvegardes du GTFS
Lorsqu'un export du fichier GTFS est réalisé (pour vérifier des erreurs par exemple) ; ce dernier peut être retrouvé directement dans la liste qui se trouve en page d'accueil du logiciel.

### 2/ Il est risqué de travailler à plusieurs sur la même instance
Travailler à plusieurs sur le même serveur de manière collaborative est impossible : chaque enregistrement écrase le précédent. Cependant :
-> il est possible d'ajouter de manière indépendante des arrêts
-> de manière générale, tant que l'on écrit sur des fichiers (agency ; stops...) différents et non interdépendants, il reste possible de saisir des données à plusieurs.

### 3/ Supprimer ou modifier des données se fait via un processus à part
Lorsque des données sont saisies par erreur, ou lorsqu'elles sont à supprimer après un premier test par exemple, il est parfois nécessaire de supprimer ou renommer des enregistrements.

Cela se fait avec un outil à part :

![](/assets/Capture_ecran_2018-11-19_15.24.53.png)

**Rename ID** permet de renommer un enregistrement (ID). On peut sélectionner un enregistrement (un trajet, un arrêt, etc...), et de le **renommer de manière identique dans toute la base** (le GTFS est une base de données relationnelle : les fichiers et les données sont interdépendants). Cet onglet permet aussi de tester le changement de nom avant de l'appliquer.

**Delete ID** permet de supprimer un enregistrement (une route, un arrêt). Cela peut entrainer **une rupture du fichier.** Exemple : si un arrêt est supprimé, et qu'il était déjà placé sur une ligne, avec des horaires, cette ligne devra être à nouveau saisie, ainsi que toutes les autres informations liées à cet arrêt. C'est pourquoi des mesures de sécurité interviennent avant la suppression de l'élément.

> **Attention** : il est donc vivement recommandé d'exporter fréquemment le fichier GTFS au fil de la saisie, et de le tester via le validateur, pour éviter de trop longues corrections.  

### 4/ Le Static GTFS Manager est un logiciel open source.
Il est gratuit et bénéficie de la contribution de ses utilisateurs. Vous pouvez vous-même suggérer des améliorations.  
