# Pourquoi un GTFS ?

----------

## Data Transport Mali : cas d'usage à la source de cette documentation

Le projet Data Transport Mali a été sélectionné dans le cadre de l’appel à projets [Outiller la communauté africaine francophone](https://aap.ogptoolbox.org/) organisé par [Johan Richer](https://twitter.com/JohanRicher) et [Jules Depeux](https://twitter.com/julesdepeux) ([OGP Toolbox](http://ogptoolbox.org/) / [Code for France](https://codefor.fr/)) en partenariat avec l’[Ecole des données (EDD)](https://fr.okfn.org/ecole-des-donnees/) et soutenu par l’[Agence française de développement (AFD)](https://www.afd.fr/fr).

## Porteurs du projet

Le projet est piloté par [Emmanuel Bama](https://twitter.com/emmanuelbama?lang=fr) et [Mohamed Konate](https://twitter.com/Mohamed_Konate_?lang=fr). Tous deux travaillent en collaboration avec d’autres membres de la communauté OpenStreetMap. Ils gèrent le portail de données en ligne [Data transport Mali](http://data-transport.org/). Emmanuel Bama est à l'origine de la communauté OSM en Côte d'Ivoire, et il est par ailleurs fondateur de l’entreprise [Billet Express](https://billetexpress.ml/), un moteur de recherche et un service de billetterie en ligne des transports routiers en Afrique de l’Ouest (5 compagnies, 70 destinations, 7 pays)

##  Objectif du projet

Sélectionné lors de l'appel à projet, Emmanuel et Mohamed ont bénéficié d'un sprint d'accompagnement, dispensé par [Johan Richer](https://twitter.com/JohanRicher) ([OGP Toolbox](https://ogptoolbox.org/)) et [Datactivist](https://datactivist.coop/fr/). **Objectif de la semaine** : générer des données de transport qui soient utilisables par un maximum de services. Cette semaine d'ateliers et de rencontres s'est déroulée entre le 5 et le 9 novembre 2018, un **GTFS** a été produit à partir de données basiques et existantes (horaires des bus et destinations, collectées par Emmanuel et Mohamed, et numérisées au format Excel pour leur gestion interne.). [En savoir plus sur ce que permet un GTFS](https://www.transitwiki.org/TransitWiki/index.php/Category:GTFS-consuming_applications).

L'outil Static GTFS Manager a été utilisé pour saisir l'ensemble des lignes et trajets, dans l'objectif de permettre à Billet Express de disposer d'un outil de gestion des lignes, de la billetterie, des itinéraires. Le GTFS qui a été produit a été validé et testé en fin de semaine dans Navitia (plateforme de services sur les données transports), et une réflexion sur les services à proposer a été menée.

![](/assets/LLLrestitution.png)
_Restitution du projet au Liberty Living Lab, d'Emmanuel Bama et Mohamed Konate_
