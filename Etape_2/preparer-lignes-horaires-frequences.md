# Préparer les lignes, les horaires et les fréquences

----------

## Cartographier les lignes (routes)
Une première étape consiste à définir l'origine-destination de la ligne, qu'on retrouvera par exemple dans le champ "route_long_name"
> Ce [template](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing) (onglet lignes) peut aider à préparer les données : ils est librement adaptable et utilisable.

Lorsque l'ordre des points de passages (arrêts) n'est pas spécifié, il faut les définir. C'est ce que l'on appelle le "stop_sequence" dans le fichier "stop_times" du GTFS.

Sur [Umap](https://umap.openstreetmap.fr/), il est possible de dessiner des points et des lignes, permettant de visualiser simplement quel est l'arrêt de départ, les arrêts intermédiaires et l'arrêt d'arrivée.

> Une autre méthode consiste à cartographier les lignes sur papier : option choisie en première étape pour le projet Data Transport Mali, utile pour mettre à plat les éventuels doutes sur les destinations, les compagnies et les horaires.

![](/assets/Capture_ecran_2018-11-13_18.06.55.png)

--------------

## Définir des trajets (trips)

Une ligne (ou route dans le langage GTFS), peut donner lieu à plusieurs trajets (trip)
Par exemple, sur la route Bamako - Abidjan, il y a un trip qui part à 8h, un autre à 9h et un autre à 15h.

Sur ce [template](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing) pour préparer les données, on déclinera donc autant de tableaux qu'il y a de trips : les allers, les retours, les différents départs...

--------------

## Attribuer les horaires (stop times)

Toujours sur le même [template](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing), indiquer les horaires de passage.
Lorsqu'elles ne sont pas connues, il faut calculer l'heure de passage à partir d'une estimation d'itinéraire (dans Google Maps par exemple).

> **Attention** : Pour faciliter la saisie plus tard dans le Static GTFS Manager et éviter les erreurs, il faut savoir que les horaires sont saisies au format 24:00:00, et que lorsque un trajet commence par exemple à 23:00:00 et qu'il arrive à 01:00:00 (01 AM) le lendemain, **l'heure d'arrivée sera 25:00:00.** Autrement dit : **l'heure de passage de l'arrêt suivant ne peut être qu'un chiffre supérieur !** Si ce n'est pas appliqué, le GTFS est invalide.

-------------

## Définir des fréquences

**Certains bus ou trains ne roulent pas tous les jours de la même manière :** il est nécessaire de définir les jours où les lignes sont en service. Dans le [template](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing), indiquer la fréquence pour chaque trajet (trip), et si des informations existent sur les jours exceptionnels (fêtes et jours fériés), les indiquer pour chaque trajet également.
