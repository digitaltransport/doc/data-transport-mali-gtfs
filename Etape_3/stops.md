# Stops

----------

> Pour l'étape des arrêts, il est pertinent de tous les saisir, pour deux raisons : faciliter et accélérer la saisie des routes, et établir une première cartographie du réseau (utile lorsqu'il existe des doutes sur la localisation ou l'ordre des arrêts). A priori, cette étape aura été préparée ici : [Préparer les arrêts](/Etape_2/preparer-les-arrêts.md)

**1/ Dans l'onglet Stops, sélectionner le volet "Add / Edit"** pour ajouter un premier arrêt

**2/ Saisir en premier le stop_id** -> c'est l'identifiant de l'arrêt, qui de préférence devrait être "évocateur", pour faciliter la gestion du fichier. Si l'arrêt s'appelle Bamako Sogoniko, choisir par exemple bamako_sogoniko

**3/ Saisir ensuite le stop_name,** qui est le nom courant de l'arrêt

**4/ Wheelchair Boarding :** indiquer si l'arrêt est accessible aux personnes en fauteuil roulant.

> **Une question  s'est posée lors du projet Data Transport Mali** : *les arrêts sont-ils accessibles aux personnes en fauteuil roulant, même si les bus ne disposent pas de la plateforme adaptée pour faire monter les fauteuils ?*

> **Réponse des porteurs de projet** -> Oui, les arrêts peuvent être marqués comme accessibles, car les passagers ou les conducteurs aident de manière systématique les personnes à mobilité réduite à embarquer aux arrêts.

**5/ Location :** latitude, longitude, comme préparé dans le template.

**6/ Zone_Id :** laisser le champ se compléter (duplication du stop_id)

> Une fois les arrêts sauvegardés dans la base de données, vous pouvez visualiser la liste et la cartographie des arrêts : **utile pour repérer d'éventuelles erreurs de coordonnées.**

![](/assets/Capture_ecran_2018-12-21_16.27.46.png)

> **Rappel :** pour corriger ou supprimer des données, cela se fait via un processus distinct : [Important avant de commencer la saisie](/Etape_3/important-avant-saisie.md)
