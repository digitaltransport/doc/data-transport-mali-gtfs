# Qu'est-ce que le Static GTFS Manager ?

----------

![](/assets/logo_static-gtfs-manager.png)

Le [Static GTFS Manager] est un outil en ligne et un logiciel **open source** (disponible sur Github), permettant d'écrire un fichier GTFS à partir de 0. Techniquement, il est possible d'écrire un fichier GTFS directement dans un tableur, mais la saisie serait laborieuse et surtout laisserait place à de nombreuses erreurs. Le GTFS Manager guide ainsi la saisie, en proposant des étapes précises, des règles de validations pour éviter les erreurs, ce qui assure un certain confort dans la saisie.

> **Static GTFS Manager ne nécessite aucune compétence particulière en informatique, si ce n'est la compréhension du vocabulaire spécifique au GTFS.**

Il peut fonctionner localement sur un ordinateur ou sur un serveur en ligne.

> Il existe d'autres outils (open source ou non) pour construire un GTFS. Le choix s'est porté sur le Static GTFS Manager pour sa facilité d'accès, cependant, il ne permet pas de saisir les données de manière collaborative. (Cf * [Important avant de commencer la saisie](/Etape_3/important-avant-saisie.md))
