# Comment vérifier les erreurs ?

----------

Une fois que l'on dispose d'un extrait de GTFS (composé du minimum de données et de fichiers obligatoires), il suffit de le télécharger sur le GTFS Feed Validator (possible également via URL sur laquelle le fichier est disponible).

Ensuite s'affiche une page relevant toutes les erreurs et avertissements :

## 1/ Les erreurs invalidantes (errors)

Pour qu'un GTFS soit fonctionnel, il faut qu'il respecte les standards : fichiers et données obligatoires, orthographe des champs, règles d'écriture des données, etc...
Ce sont les erreurs à corriger en priorité.

**Exemples :**

* le bus part avant d'arriver à l'arrêt "xxx" sur le trajet "xxx"

* le bus arrive à destination avant d'être parti de son origine (cela arrive souvent lorsque l'on a saisi par exemple en heure d'arrivée "02:00:00" au lieu de "26:00:00" (cf [Préparer les lignes, les horaires et les fréquences](/Etape_2/preparer-lignes-horaires-frequences.md))

* le fichier "stop_times.txt" est manquant

## 2/ Les avertissements (warnings)

Pour les autres éléments repérés dans le validateur, il s'agit d'avertissements, plus ou moins importants à prendre en compte.

1. Avertissements "sévères", généralement à corriger : il peut s'agir d'arrêts qui sont géolocalisés trop proches (le validateur indique donc une vitesse modélisée entre ces deux arrêts, qui est bien trop haute pour être possible).

2. Avertissements à prendre en compte ou à laisser de côté : l'absence d'exceptions de service par exemple, lorsqu'on indique qu'un bus roule tous les jours de l'année sans exception.
