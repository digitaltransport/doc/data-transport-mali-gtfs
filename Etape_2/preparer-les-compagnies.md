# Préparer le fichier des compagnies de transport

----------

* Si le réseau est composé d'une seule compagnie de transport ("agency" en langage GTFS), passer à l'étape suivante (préparer les arrêts)

* Si le réseau est composé de plusieurs compagnies, il est pertinent de constituer un tableau à part avec les compagnies et leur URL.

* Autrement, on peut renseigner la compagnie dans le [template](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing) (onglet destiné aux horaires), dans l'entête de la ligne :

![](/assets/Capture_ecran_2018-11-14_12.27.16.png)
