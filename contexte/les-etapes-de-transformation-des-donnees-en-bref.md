# Les étapes de transformation des données

----------

Pour passer du fichier original à un standard GTFS, plusieurs étapes ont été nécessaires, elles seront détaillées dans cette documentation.

###  Simplifier les données

Dans le fichier de base, qui ne répondait à aucun standard, les données sur les itinéraires de bus étaient par exemple déclinées pour chaque jour du mois.

> 1/08 ; 2/08 ; 5/08 ; 6/08 ; ...

Pour mieux correspondre au GTFS, et de manière plus générale, pour mieux comprendre les données, une des étapes a été de définir des fréquences pour chaque ligne de bus.

> Tous les jours ; lundi - jeudi et samedi ; tous les jours sauf le dimanche

###  Ajouter les données manquantes, cartographier le réseau

La première nécessité a été de localiser les arrêts : le GTFS demande des coordonnées géographiques, qu'il a fallu associer à chaque arrêt.

Cela nous a aussi mené à cartographier les différentes lignes, pour comprendre l'ordre des arrêts, et permettre de calculer des heures de passages des bus.

| GANA 4 *(nom de la ligne)* | Tous les jours sauf le dimanche *(fréquence)* |
|---|---|
| Bamako Medine | 06:00:00 |
| Bobo Dioulasso | 15:00:00 |
| Ouagadougou | 21:00:00 |

### Saisir les données au format GTFS

Une fois les données bien claires, et complètes, il a fallu les saisir dans un outil nous permettant d'écrire directement dans un standard GTFS.

L'outil choisi est [Static GTFS Manager](https://github.com/WRI-Cities/static-GTFS-manager), nous détaillons son utilisation dans cette documentation.
