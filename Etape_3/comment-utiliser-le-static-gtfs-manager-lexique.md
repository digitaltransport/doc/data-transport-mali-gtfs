# Comment utiliser le Static GTFS Manager : lexique de base

----------

> Avant tout, voici un lexique (non exhaustif) des termes que l'on va retrouver dans le Static GTFS Manager. [Cette introduction au GTFS](https://github.com/datactivist/datatransportmali/blob/master/Introduction%20au%20GTFS.pdf), réalisée dans le cadre du projet [Data Transport Mali](/contexte/le-projet-data-transport-mali-a-la-source-de-la-documentation.md#data-transport-mali-cas-dusage-a-la-source-de-cette-documentation) est plus exhaustive. Et pour la totalité des spécifications, consulter la [documentation de Google](https://developers.google.com/transit/gtfs/reference/?hl=fr).

| **Terme utilisé** | Traduction GTFS | Explication |
|---|---|---|
| **Agency** | Compagnie | Compagnie ou autorité effectuant les trajets |
| **Fare** | Prix | Prix des trajets et règles associées (facultatif) |
| **Headsign** | Panneau directionnel | Panneau digital qui indique la destination ou le nom de l'itinéraire |
| **id** | Identifiant | Il identifie de manière unique un élément du GTFS pour pouvoir être lié à d'autres données |
| **Route** | Ligne | Origine destination pouvant donner lieu à plusieurs trajets différents (horaires différents), mais associé à une compagnie. |
| **Sequence** | Ordre | Ordre des arrêts (stop_sequence) (arrêt de départ, intermédiaire ou arrivée) |
| **Service** | Calendrier | Jours où les trajets sont effectués |
| **Shape** | Tracé | Tracé en shapefile des lignes (facultatif) |
| **Timepoint** | Marqueur temporel | Si l'arrêt est déterminant en termes d'horaire, le conducteur attendra l'heure de départ exacte avant de partir. Sinon, c'est un horaire de passage estimé.  |
| **Timezone** | Fuseau horaire | Utile sur les réseaux internationaux |
| **Trip** | Trajet ou itinéraire | Aller OU retour, avec des heures et un calendrier définis |
| **Wheelchair** | Fauteuil roulant | Décrit l'accessibilité du réseau (facultatif) |
