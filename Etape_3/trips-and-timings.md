# Trips and Timings

----------

## 1/ Créer un Trip**

Le trip est un trajet unique, effectué de manière régulière, à un horaire défini (La route est quant à elle une origine - destination pouvant donner lieu à plusieurs trips).

1. Sélectionner une **route** - dont l'ordre des arrêts aura été défini à cette étape)

2. Choisir un **calendrier** (les jours lors desquels le trajet est effectué, décrit à cette étape)

3. Choisir une **heure de départ** (l'heure à laquelle le bus ou le train part du premier arrêt)

4. Choisir une **direction**

    * aller
    * retour
    * aucune (dans le cas où le retour n'est pas connu ou qu'il est indépendant de l'aller)
    * les deux (dans le cas de lignes qui font des boucles)

5. **Ajouter le trip**

6. **Sauvegarder**

> On remarque que Static GTFS Manager attribue automatiquement un trip_id, qui reprend le route_id.

## 2/ Attribuer des horaires**

> Puisque les arrêts ont été attribués aux routes, et donc aux trips, il ne reste plus qu'à leur attribuer des horaires de passage.

1. Sélectionner le trip

2. Charger les horaires : les arrêts apparaissent dans l'ordre (qui a été écrit précédemment).

![](/assets/click_to_load.png)

3. Saisir les horaires de passage

*   *arrival_time* = l'heure à laquelle le bus arrive à l'arrêt pour récupérer les passagers

*   *departure_time* = l'heure à laquelle le bus part de l'arrêt avec les passagers

* Pour plus de facilité, il est aussi possible d'indiquer la même heure pour 'arrival_time' et 'departure_time'.


4. Qualifier le caractère estimé ou précis de l'heure de passage :

*   0 = horaire estimé : n'est pas un arrêt déterminant dans le déroulé du parcours (le bus n'attend pas une heure spécifique pour repartir de l'arrêt)
*   1 = horaire exact, ou du moins à respecter.


5. Sauvegarder les horaires de passage
