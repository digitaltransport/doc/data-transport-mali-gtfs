# À quoi ressemble un GTFS ?

----------

> Je sais ce dont j'ai besoin pour constituer un fichier GTFS, mais à quoi ressemblent les fichiers qui le composent une fois qu'il est terminé? 

### Exemple du GTFS de Billet Express 

Cet exemple de GTFS est un prototype réalisé lors d'une semaine de sprint. Il comprend 5 compagnies, 37 lignes, 66 arrêts et 191 heures de passages. 

Pour plus de confort, il est possible de le visualiser sous forme de tableur : téléchargez l'[exemple de GTFS ici](https://git.digitaltransport4africa.org/places/mali/raw/master/GTFS/GTFS.zip?inline=false)  et ouvrez les différents fichiers .txt avec un logiciel de tableur (via libre office, excel, calc, etc...)