# Préparer les arrêts

----------

Ici, l'objectif est de disposer d'une liste d'arrêts ("stops") avec leurs *coordonnées géographiques*. 

Voici un [template](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing) pour vous aider à les renseigner, dans le cas où les données géographiques ne sont pas encore collectées.
