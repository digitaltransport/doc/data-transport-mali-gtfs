# Ressouces utiles

----------

### Ressources sur le format GTFS

* [Introduction au format GTFS](https://github.com/datactivist/datatransportmali/blob/master/Introduction%20au%20GTFS.pdf)

* [Spécifications complètes du format GTFS (documentation Google)](https://developers.google.com/transit/gtfs/reference/?hl=fr)

* [Transit Wiki : GTFS consuming applications (anglais)](https://www.transitwiki.org/TransitWiki/index.php/Category:GTFS-consuming_applications)

* [Awesome Transit : liste de ressources sur les données transports](https://github.com/CUTR-at-USF/awesome-transit)


### Les Templates à réutiliser pour simplifier la saisie des données

* [Template sur les arrêts (premier onglet)](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing)

* [Template sur les lignes, compagnies (deuxième onglet)](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing)

