# Pourquoi et comment préparer les données ?

----------

Si des données existent déjà (horaires en PDF, fichiers excel, etc...), **il faut s'assurer qu'elles seront facilement transposables dans l'outil Static GTFS Manager, et donc les simplifier.**

## Quelles données préparer au préalable ?

Pour cela, on peut passer en revue cette check list : si ces 6 points sont validés, on peut passer directement à la saisie du GTFS

1. Je dispose des noms des compagnies et connais les lignes de transport dont elles ont la charge

2. Je dispose d'une liste des arrêts et stations, avec leurs coordonnées géographiques (format WGS84)

3. Je connais les lignes de transport et l'ordre des arrêts qu'elles desservent

4. Je connais les horaires des différents départs sur chaque ligne de transport

5. Je dispose de données facilement lisibles sur les horaires de passage à chaque arrêt pour chaque ligne, pour chaque trajet (départ de 9h, départ de 11h, etc...)

6. Je connais la fréquence de chaque ligne de transport et les jours où il n'y a pas de service

Et donc, si tous les points sont validés, on passe directement à [Qu'est-ce que le Static GTFS Manager ?](/Etape_3/Quest-ce-que-le-static-gtfs-manager.md)

Et si la checklist n'est pas complète, poursuivre ci-dessous.

> Comme mentionné précédemment, un GTFS peut contenir beaucoup d'informations supplémentaires, mais elles sont facultatives : nous les écartons pour le moment. D'autre part, cette checklist permet d'anticiper les principaux manques de données, mais d'autres opérations seront nécessaires pour constituer le GTFS.

## Comment préparer ou simplifier les données ?

Reprise des différents points  :

1. Je dispose des noms des compagnies et connais les lignes de transport dont elles ont la charge
  * [Préparer les compagnies](/Etape_2/preparer-les-compagnies.md)

2. Je dispose d'une liste des arrêts et stations, avec leurs coordonnées géographiques (format WGS84)
  * [Préparer les arrêts](/Etape_2/preparer-les-arrêts.md)

3. Je connais les lignes de transport et l'ordre des arrêts qu'elles desservent
  * [Cartographier les lignes](/Etape_2/preparer-lignes-horaires-frequences.md#cartographier-les-lignes-routes)

4. Je connais les horaires des différents départs sur chaque ligne de transport
  * [Définir les trajets](/Etape_2/preparer-lignes-horaires-frequences.md#définir-des-trajets-trips)

5. Je dispose de données facilement lisibles sur les horaires de passage à chaque arrêt pour chaque ligne, pour chaque trajet (départ de 9h, départ de 11h, etc...)
  * [Attribuer les horaires](/Etape_2/preparer-lignes-horaires-frequences.md#attribuer-les-horaires-stop-times)

6. Je connais la fréquence de chaque ligne de transport et les jours où il n'y a pas de service
  * [Définir les fréquences](/Etape_2/preparer-lignes-horaires-frequences.md#définir-des-fréquences)
