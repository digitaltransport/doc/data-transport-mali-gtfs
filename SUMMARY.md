# Summary

* [Introduction](README.md)

### CONTEXTE ET INTRODUCTION

 * [Pourquoi un GTFS ?](contexte/le-projet-data-transport-mali-a-la-source-de-la-documentation.md)
 	* [Les données de départ, les données d'arrivée](contexte/les-donnees-de-depart-les-donnees-darrivee.md)
 	* [Les étapes de transformation des données](contexte/les-etapes-de-transformation-des-donnees-en-bref.md)

### Etape 1 : IDENTIFIER ET COLLECTER LES DONNÉES

* [De quelles données a-ton besoin pour construire un GTFS ?](Etape_1/de-quelles-données-a-t-on-besoin-pour-construire-un-GTFS.md)
    * [A quoi ressemble un GTFS ?](Etape_1/a-quoi-ressemble-un-GTFS.md)
    * [Comment collecter les données ?](Etape_1/comment-collecter-les-données.md)

### Etape 2 : PRÉPARER ET SIMPLIFIER LES DONNÉES

* [Pourquoi et comment préparer les données ?](Etape_2/pourquoi-et-comment-preparer-les-donnees.md)
    * [Préparer les compagnies](Etape_2/preparer-les-compagnies.md)
    * [Préparer les arrêts](Etape_2/preparer-les-arrêts.md)
    * [Préparer les lignes, les horaires et les fréquences](Etape_2/preparer-lignes-horaires-frequences.md)

### Etape 3 : SAISIR LES DONNÉES DANS LE GTFS

* [Qu'est-ce que le Static GTFS Manager ?](Etape_3/Quest-ce-que-le-static-gtfs-manager.md)
    * [Comment installer le Static GTFS](Etape_3/comment-installer-static-gtfs-manager.md)
    * [Comment utiliser le Static GTFS Manager : lexique de base](Etape_3/comment-utiliser-le-static-gtfs-manager-lexique.md)
* [Important avant de commencer la saisie](Etape_3/important-avant-saisie.md)
    * [Agency](Etape_3/agency.md)
    * [Stops](Etape_3/stops.md)
    * [Routes](Etape_3/routes.md)
    * [Calendar](Etape_3/calendar.md)
    * [Trips and timings](Etape_3/trips-and-timings.md)

### VALIDER ET CORRIGER LES DONNÉES

* [Qu'est-ce que le GTFS Feed Validator ?](Valider et corriger les données/gtfs-feed-validator.md)
    * [Comment vérifier les erreurs ?](Valider et corriger les données/comment-verifier-erreurs.md)

### RESSOURCES

* [Ressources utiles](Ressources/ressources-utiles.md)
