# Comment collecter les données ?

----------

### Dans le cas où les données sont détenues par des tiers :

> *Je ne dispose d'aucune donnée, comment les collecter auprès des compagnies ?*

Pour faciliter le travail de collecte et éviter les pertes de temps, une des solutions peut consister à envoyer des templates à remplir par les compagnies.  Ces templates peuvent être des fiches décrivant précisément les lignes, et les données n'auraient plus qu'à être saisies dans le Static GTFS Manager (étape d'après).

![](/assets/Capture_ecran_2018-11-14_14.15.15.png) ![](/assets/Capture_ecran_2018-11-14_12.27.16.png)

> Vous trouverez [des templates déjà construits ici](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing), librement adaptables et réutilisables (un onglet arrêts, un onglet lignes)

> *Les compagnies m'ont indiqué les lieux de passages (arrêts) mais je ne connais pas leur géolocalisation précise (coordonnées)*

Il faut dans ce cas déterminer soi-même les coordonnées géographiques.

Pour cela : deux sources principales sont à interroger : Open Street Map et Google Maps
Sur [Open Street Map](https://www.openstreetmap.org/), il suffit d'effectuer un clic droit sur le point choisi de la carte, et de sélectionner "Afficher l'adresse" et de récupérer les coordonnées qui s'affichent.
Sur [Google Maps](https://www.google.com/maps), il faut effectuer un clic droit, et sélectionner "Plus d'infos sur cet endroit"

### Dans le cas où les données vous appartiennent
> Je dispose de mes propres bases de données, mais elles ne sont pas formatées : comment en faire un GTFS ?

Avant de passer directement au format GTFS, il faut s'assurer qu'elles seront saisies sans difficulté dans le Static GTFS Manager.  
[Pourquoi et comment préparer les données ?](/Etape_2/pourquoi-et-comment-preparer-les-donnees.md)

> Je ne dispose d'aucune donnée informatique, mais je sais localiser les arrêts et j'ai une idée des horaires de passage.

Dans ce cas l'une des options peut-être de localiser les arrêts directement sur Open Street Map. Le [Guide du débutant d'Open Street Map](https://wiki.openstreetmap.org/wiki/FR:Guide_du_d%C3%A9butant) permet de comprendre comment contribuer et ajouter des données. Il est possible de collecter les informations directement sur le terrain avec un GPS.

Concernant les horaires, le [template suivant](https://docs.google.com/spreadsheets/d/1L5cFidCZnYG4_-7F5isvWEmN1yHcS-s73keqjhL3Oqk/edit?usp=sharing) vous permet de préparer les données en toute simplicité.
