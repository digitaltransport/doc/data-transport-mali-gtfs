# Routes

----------

Rappel de la définition de "Route" dans le GTFS : origine destination pouvant donner lieu à plusieurs trajets différents (horaires différents), mais associé à une compagnie.

-----------

**1/ Choisir la compagnie** qui effectue ce trajet

**2/ Ajouter les informations** correspondant à la route : 

* **route_long_name**, qui peut être par exemple " origine-destination "

* **route_type**, soit"3" si c'est un bus qui effectue l'itinéraire  (voir la documentation Google) 

*  éventuellement, **les informations facultatives** comme sur la couleur de la ligne et du texte 

**3/ Renseigner l'ordre des arrêts** sur l'itinéraire

--------

> Recommandé : c'est à cette étape de la saisie qu'il est pertinent d'attribuer les stops (et leur ordre) aux routes. En effet, ce que l'on appelle stop_sequence se trouve dans le fichier stop_times mais la saisie à cette étape permettra de gagner du temps et limiter les erreurs

* Sélectionner la route 

* Saisir les arrêts dans l'ordre (s'ils ont bien été écrits et sauvegardés en premier lieu)

* Dupliquer (et inverser) l'ordre des arrêts pour le trajet retour (laisser vide cette partie "Return Journey si le trajet retour n'est pas connu)

* (facultatif) Importer un shapefile : le tracé géographique de la ligne. 

> Le shapefile, à terme dans le fichier shapes et facultatif mais permet des visualisations plus esthétiques du réseau. En effet, sans les shapefiles, les lignes relient uniquement les arrêts, sans prendre en compte le tracé réel des routes physiques. 

--------

**4/ Sauvegarder**

-------

**5/ (Rappel) Vérifier si les données ont bien été saisies,** en se rendant sur la page d'accueil (Home).