# Construire un jeu de données GTFS

## Contexte

Ce guide est rédigé [dans le cadre du projet **Data Transport Mali**][1], et s'appuie ainsi sur un exemple réel qui a suivi cette méthodologie. Il a vocation à être amélioré au fil de l'évolution des projets qui suivent cette méthodologie.

Il est publié sous licence CC0 ([Creative Commons Zéro](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)) : toute personne est autorisée à l'utiliser, le modifier et le partager librement.

Son contenu est hébergé dans un dépôt Git sur le centre de ressources [Digital Transport for Africa](http://digitaltransport4africa.org/) à l'adresse suivante :  
**https://git.digitaltransport4africa.org/doc/data-transport-mali-GTFS**.

Le guide est consultable sur le site [doc.digitaltransport.io/data-transport-mali-GTFS](http://doc.digitaltransport.io/data-transport-mali-GTFS/), avec mise à jour automatique à parti du dépôt Git (intégration continue, Gitlab Pages).

Toute proposition de correction et d'amélioration est bienvenue !

Vous trouverez le jeu de données GTFS de Data Transport Mali créé lors de la formation sur [ce dépôt](https://git.digitaltransport4africa.org/places/mali).

## Objet

Cette documentation a pour objet la construction d'un jeu de données au format GTFS (_General Transit Feed Specification_), à partir de 0. Elle détaille les étapes nécessaires à l'identification, la collecte, la standardisation, l'ouverture en open data et l'utilisation de données de transport :

 - Horaires
 - Arrêts
 - Lignes et itinéraires
 - Calendriers de service
 - Tarifs

## Important

Il ne s'agit pas d'un résumé de la [documentation complète du format par Google][2], le but est d'expliquer de manière concrète la construction d'un eju de données GTFS et vise à répondre à l'ensemble des questions qui peuvent se poser avant la construction, jusqu'au moment où le fichier est exploitable.

Il est proposé ici d'utiliser (entre autres) l'outil open source **Static GTFS Manager** pour générer le fichier en question : une partie de la documentation lui sera dédiée. Voici en complément une [introduction au format GTFS ][3]réalisée dans le cadre du projet.       

> Cette documentation propose de construire pas à pas le GTFS, mais il est possible de passer dès maintenant à [l'étape de saisie][4] : soit parce que vous disposez déjà de données suffisamment structurées, soit parce que vous souhaitez comprendre ce qui vous manque et ce que cela implique, en testant directement l'outil. **C'est une bonne façon de s'approprier le sujet en testant**, vous pourrez toujours revenir aux étapes de base par la suite.

[1]: /contexte/le-projet-data-transport-mali-a-la-source-de-la-documentation.md
[2]: https://developers.google.com/transit/gtfs/reference/?hl=fr
[3]: https://github.com/datactivist/datatransportmali/blob/master/Introduction%20au%20GTFS.pdf
[4]: /Etape_3/Quest-ce-que-le-static-gtfs-manager.md
