# Comment installer le Static GTFS Manager ?

----------

[Lien vers  l'installation sur Linux, Mac et Windows](https://github.com/WRI-Cities/static-GTFS-manager#run-on-your-system)

Spécifiquement pour Mac, [cette page](https://github.com/WRI-Cities/static-GTFS-manager/issues/130) explique une installation simplifiée :

### Traduction :

* [installer Docker](https://store.docker.com/editions/community/docker-ce-desktop-mac)

saisir dans le terminal :

    git clone https://github.com/WRI-Cities/static-GTFS-manager

puis à nouveau, saisir dans le terminal :

    docker build -t wri-cities/static-gtfs-manager .
    docker run -it -p 5000:5000 -v persistent:/app/db "wri-cities/static-gtfs-manager"
    
* Ouvrir [http://localhost:5000/](http://localhost:5000/) dans le navigateur