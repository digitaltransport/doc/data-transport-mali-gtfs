# Calendar

----------

Le fichier calendar.txt est celui où l'on décrit les jours lors desquels le service de transport fonctionne. Lorsque par exemple un bus fonctionne tous les jours sauf le dimanche, il faudra associer ce trajet à un identifiant de calendrier correspondant. 

**1/ Créer un nouveau calendrier :** ce sera le service_id. Pour plus de facilité, le nom donné à l'identifiant du calendrier peut se composer des initiales des jours de service : 
*mo-tu-we-th-fr-sa-su* = le bus roule tous les jours
*mo-th-sa* = le bus roule le lundi, jeudi et samedi

**2/ Indiquer les jours de service :** 1 = service opérant ; 0 = pas de service

**3/ Sauvegarder**

> Dans cette version "minimale" du GTFS, on ne définit pas d'exceptions de services. C'est à dire que lorsqu'on indique par exemple qu'un bus roule tous les jours sauf le dimanche, il effectue ce service toute l'année, sans prendre en compte les jours fériés, fêtes religieuses, périodes de vacances, etc... Ces exceptions peuvent être définies dans un second temps, dans calendar_dates. 