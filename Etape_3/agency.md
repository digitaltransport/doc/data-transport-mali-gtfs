# Agency

----------

> Rappel : le Static GTFS Manager se décompose en plusieurs onglets : Agency, Stops, Routes etc... Il est préférable de procéder à la saisie dans l'ordre proposé.

**1/ Dans l'onglet Agency,** ajouter un agency_id (identifiant de la compagnie), pour commencer à entrer les données sur une première compagnie. Choisir une abréviation pour cet identifiant, comme les 3 premières lettres de la compagnie.

**2/ Puis, saisir directement les informations demandées sur les compagnies** (nom, URL, fuseau horaire).

**3/ Sauvegarder**

![](/assets/save_agency_changes.png)

**4/ Vérifier si les données ont bien été saisies,** en se rendant sur la page d'accueil (Home). Normalement, sous "Main tables", la ligne "agency"  devrait présenter  "1 entry" (soit un enregistrement : la compagnie que vous venez de saisir).

![](/assets/Capture_ecran_2018-12-21_12.16.42.png)

> Rappel : l'outil propose de saisir le minimum nécessaire de données pour que le GTFS soit fonctionnel. Selon le projet de service et selon vos objectifs, d'autres champs pourraient être à compléter par la suite.
